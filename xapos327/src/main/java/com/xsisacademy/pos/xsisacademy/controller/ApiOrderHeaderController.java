package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;
import com.xsisacademy.pos.xsisacademy.repository.OrderHeaderRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderHeaderController {
	
	//anotasi autwired untuk menyhambung repo
	@Autowired
	private OrderHeaderRepository orderHeaderRepository;
	
	@PostMapping("orderheader/create")
	public ResponseEntity<Object> createReference(){
		OrderHeader orderHeader = new OrderHeader();
		
		String timeDec = String.valueOf(System.currentTimeMillis());
		
		orderHeader.reference = timeDec;
		orderHeader.amount = 0;
		orderHeader.isActive = true;
		orderHeader.createBy = "admin1";
		orderHeader.createDate = new Date();
		
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		
		if(orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Create Succes", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Create Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeader() {
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId,HttpStatus.OK);
			
		}
		catch(Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("orderheadercheckout/{headerId}/{totalAmount}")
	public ResponseEntity<Object> checkout(@PathVariable("headerId") Long headerid, @PathVariable("totalAmount") double totalAmount){
		try {
			OrderHeader orderHeaderData = this.orderHeaderRepository.findById(headerid).orElse(null);
			
			orderHeaderData.amount = totalAmount;
			orderHeaderData.modifyBy = "admin1";
			orderHeaderData.modifyDate = new Date();
			this.orderHeaderRepository.save(orderHeaderData);
			return new ResponseEntity<>("Checkout success", HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		}
	}
	

}
