package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Variants;
import com.xsisacademy.pos.xsisacademy.repository.VariantsRepository;

@RestController         //Rest sebuah API 
@RequestMapping("/api/")
public class ApiVariantsController {
	
		@Autowired
		private VariantsRepository variantsRepository;
		
		@GetMapping("variants")
		public ResponseEntity<List<Variants>> getAllVariants() {
			
			try {
				List<Variants> listVariants = this.variantsRepository.findByVariants();      //buat fungsi,get data dari jpa repository
				return new ResponseEntity<>(listVariants, HttpStatus.OK);
			}
			catch(Exception e) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			
		}
		
		@PostMapping("variants/add")
		public ResponseEntity<Object> saveVariants(@RequestBody Variants variants){
			variants.setCreateBy("admin1"); //dari fungsi getter setter
			variants.setCreateDate(new Date());
			Variants variantsData = this.variantsRepository.save(variants);
			if (variantsData.equals(variants)) {
				return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
			}
			else {
				return new ResponseEntity<>("Save Failed", HttpStatus.OK);
			}
		}
		
		@GetMapping("variants/{id}") //PAKE PATH VARIABLE
		public ResponseEntity<Object> getVariantsById(@PathVariable Long id){  //TYPE DATA
			try {
				Optional<Variants>variants = this.variantsRepository.findById(id); //Ambil data dari database (id)
				return new ResponseEntity<>(variants, HttpStatus.OK);
			}
			catch(Exception e) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}
		
		@PutMapping("variants/edit/{id}")
		public ResponseEntity<Object>editVariants(@PathVariable("id") Long id, @RequestBody Variants variants) {
			Optional<Variants> variantsData = this.variantsRepository.findById(id);
			
			if(variantsData.isPresent()) {
				variants.id = id;
				variants.modifyBy = "admin1";
				variants.createBy = variantsData.get().createBy;
				variants.createDate = variantsData.get().createDate;
				
				this.variantsRepository.save(variants);
				return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		
		@PutMapping("variants/delete/{id}")
		public ResponseEntity<Object> deleteVariants(@PathVariable("id") Long id){
			Optional<Variants> variantsData = this.variantsRepository.findById(id);
			
			if(variantsData.isPresent()) {
				Variants variants = new Variants();
				variants.id = id;
				variants.categoryId = variantsData.get().categoryId;
				variants.isActive = false;
				variants.modifyBy = "admin1";
				variants.modifyDate = new Date();
				variants.createBy = variantsData.get().createBy;
				variants.createDate = variantsData.get().createDate;
				variants.variantsInitial = variantsData.get().variantsInitial;
				variants.variantsName = variantsData.get().variantsName;
				
				this.variantsRepository.save(variants);
				return new ResponseEntity<>("Deleted Data Success", HttpStatus.OK);
			}
			else {}
			return ResponseEntity.notFound().build();
			
		}
}