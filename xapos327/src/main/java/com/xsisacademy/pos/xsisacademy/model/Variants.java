package com.xsisacademy.pos.xsisacademy.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "variants")
public class Variants {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;
	
	@Column(name = "category_id")
	public Long categoryId; //categoryId (objek)
	
	@Column(name = "variants_initial")
	public String variantsInitial;
	
	@Column(name = "variants_name")
	public String variantsName; //field
	
	@Column(name = "is_active")
	public Boolean isActive;
	
	@Column(name = "create_by")
	public String createBy;
	
	@Column(name = "create_date")
	public Date createDate;
	
	@Column(name = "modify_by")
	public String modifyBy;
	
	@Column(name = "modify_date")
	public Date modifyDate;
	
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)  //joincolumn dari tabel lain  //PROPERTIES AJA INSERT dan UPDATE
	public Category category; //object kelas nya

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getVariantsInitial() {
		return variantsInitial;
	}

	public void setVariantsInitial(String variantsInitial) {
		this.variantsInitial = variantsInitial;
	}

	public String getVariantsName() {
		return variantsName;
	}

	public void setVariantsName(String variantsName) {
		this.variantsName = variantsName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	
	
	

}
