package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	
	//Menggunakan JpaRespository
	List<Product> findByIsActive(Boolean isActive);

	//Menggunakan 2 Parameter contoh pakai java class pakai sintaks postgre sql 
	//@Query(value = "select p from Product p where p.isActive = ? 1 and p.productInitial like = '%?2%' order by p.productName")
	//List<Product> findByIsActive(Boolean isActive, String productInitial);
	
	//jika menggunakan sql asli (native query)
	//@Query(value = "select p from Product p where p.isActive = ? 1 and p.productInitial like = '%?2%' order by p.productName", native query = true)
	//List<Product> findByIsActive(Boolean isActive, String productInitial);
}
