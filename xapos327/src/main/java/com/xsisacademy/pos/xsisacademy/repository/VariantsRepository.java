package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Variants;

public interface VariantsRepository extends JpaRepository<Variants, Long> {
	@Query(value = "SELECT v FROM Variants v WHERE is_active = true")  //native query
	List<Variants> findByVariants();  
	
	@Query(value = "select v FROM Variants v where v.isActive = true and v.categoryId = ?1")
	List<Variants> findByCategory(Long categoryId);
	
	//Menggunakan JpaRepository
//	List<Variants> findIsActive(Boolean);

}
