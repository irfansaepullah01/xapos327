package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/role/")
public class RoleController {

//USING API//
@GetMapping("indexapi")
public ModelAndView indexapi() {
	ModelAndView view = new ModelAndView("role/indexapi.html");
	return view;
	}
}
