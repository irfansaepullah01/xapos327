package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsisacademy.pos.xsisacademy.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {  //nama class dan id

	List<OrderDetail> findOrderByHeaderId(Long id);
}
