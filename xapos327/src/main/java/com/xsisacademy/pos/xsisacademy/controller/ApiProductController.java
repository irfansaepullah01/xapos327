package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Product;
import com.xsisacademy.pos.xsisacademy.model.Variants;
import com.xsisacademy.pos.xsisacademy.repository.ProductRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantsRepository;

@RestController
@RequestMapping("/api/")   //dari index api sudah
public class ApiProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantsRepository variantsRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> listProduct = this.productRepository.findByIsActive(true);     //(true, " "); untuk di repo yang query
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		}
		catch (Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("product/variantsbycategoryid/{categoryId}")
	public ResponseEntity<List<Variants>> getVariantByCategoryId(@PathVariable("categoryId")Long categoryId) {
		try {
			System.out.println(categoryId);
			List<Variants> listVariants = this.variantsRepository.findByCategory(categoryId);
			return new ResponseEntity<>(listVariants, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.createBy = "admin1";
		product.createDate = new Date();
		
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)){
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Data Failed", HttpStatus.OK);
		}
	}
	

	@GetMapping("product/{id}") //PAKE PATH VARIABLE
	public ResponseEntity<Object> getVariantsById(@PathVariable Long id){  //TYPE DATA
		try {
			Optional<Product>product = this.productRepository.findById(id); //Ambil data dari database (id)
			return new ResponseEntity<>(product, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("product/edit/{id}")
	public ResponseEntity<Object>editProduct(@PathVariable("id") Long id, @RequestBody Product product) {
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			product.id = id;
			product.modifyBy = "admin1";
			product.createBy = productData.get().createBy;
			product.createDate = productData.get().createDate;
			
			this.productRepository.save(product);
			return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("product/delete/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id){
		Optional<Product> productData = this.productRepository.findById(id);
		
		// kalo ga pake optional ga perlu di tambahin pakainya or else
		
		if(productData.isPresent()) {
			Product product = new Product();
			product.id = id;
			product.variantId = productData.get().variantId;
			product.isActive = false;
			product.modifyBy = "admin1";
			product.modifyDate = new Date();
			product.createBy = productData.get().createBy;
			product.createDate = productData.get().createDate;
			product.productInitial = productData.get().productInitial;
			product.productName = productData.get().productName;
		
			
			this.productRepository.save(product);
			return new ResponseEntity<>("Deleted Data Success", HttpStatus.OK);
		}
		else {}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping("product/checkout/{productId}/{stockLeft}")
	public ResponseEntity<Object> productCheckout(@PathVariable("productId") Long id,
			@PathVariable("stockLeft") double stock) {
		try {
			Product productData = this.productRepository.findById(id).orElse(null);
			productData.modifyBy = "Admin 1";
			productData.modifyDate = new Date();
			productData.stock = stock;
			this.productRepository.save(productData);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
