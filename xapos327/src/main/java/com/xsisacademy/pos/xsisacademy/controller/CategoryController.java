package com.xsisacademy.pos.xsisacademy.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.VMCategory;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {// punya controller

	// manggil setelah di repository
	@Autowired // setelah bikin model
	private CategoryRepository categoryRepository; // dapat dari repository

	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/img";

	@GetMapping("index")
	public ModelAndView index() { // punya fungsi
		ModelAndView view = new ModelAndView("category/index.html"); // folder,file return sebuah view yang mengirimkan
																		// data model

		// ambil data dari database
		List<Category> listCategory = this.categoryRepository.findAll(); // simpan ke list

		// lempar data ke view
		view.addObject("listCategory", listCategory); // nambahin ke view

		return view;
	}

	@GetMapping("addform") // http nya get
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("/category/addform.html");

		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

//	@PostMapping("save") //http nya post (biar nggak terlihat)
//	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
//		//untuk create
//		if (!result.hasErrors()) { 
//			if (category.id == null) { //untuk id pasti null
//				category.createBy = "admin1";
//				category.createDate = new Date();
//				//untuk edit
//			} else {
//				Category tempCategory = this.categoryRepository.findById(category.id).orElse(null);
//				if (tempCategory != null) {
//					category.createBy = tempCategory.createBy;
//					category.createDate = tempCategory.createDate;
//					category.modifyBy = "admin1";
//					category.modifyDate = new Date();
//				}
//			}
//			this.categoryRepository.save(category);
//			return new ModelAndView("redirect:/category/index");
//
//		} else {
//			return new ModelAndView("redirect:/category/index");
//		}
//	}

	@PostMapping("save") // http nya post (biar nggak terlihat)
	public ResponseEntity<Object> save(@ModelAttribute VMCategory vmcategory, BindingResult result) {
		// untuk create
		if (!result.hasErrors()) {
			LocalDateTime dateTime = LocalDateTime.now();
			DateTimeFormatter formatdate = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

			String filename = dateTime.format(formatdate) + "_" + vmcategory.file.getOriginalFilename();

			Path fileNameAndPath = Paths.get(uploadDirectory, filename);

			try {
				Files.write(fileNameAndPath, vmcategory.file.getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}
			Category category = new Category();

			if (vmcategory.id == null) { // untuk id pasti null
				category.categoryInitial = vmcategory.categoryInitial;
				category.categoryName = vmcategory.categoryName;
				category.isActive = vmcategory.isActive;
				category.createBy = "admin1";
				category.createDate = new Date();
				category.sphoto = filename;
				// untuk edit
			} else {
				Category tempCategory = this.categoryRepository.findById(vmcategory.id).orElse(null);
				if (tempCategory != null) {
					category = tempCategory;
					category.categoryInitial = vmcategory.categoryInitial;
					category.categoryName = vmcategory.categoryName;
					// category.createBy = tempCategory.createBy; ngga usah (tanya leo yang option)
					// category.createDate = tempCategory.createDate;
					category.modifyBy = "admin1";
					category.modifyDate = new Date();
					category.sphoto = filename;
				}
			}
			this.categoryRepository.save(category);
			return new ResponseEntity<>(category, HttpStatus.OK);

		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("CheckNameIsExist")
	 public ResponseEntity<Object> CheckNameIsExist(@RequestParam("name") String categoryName,
	            @RequestParam("id") Long id) {

	        Boolean isExist = false;
	        Category category = null;

	        if (id == 0) {
	            category = this.categoryRepository.findByIdName(categoryName);
	        }

	        else {
	            category = this.categoryRepository.findByIdNameForEdit(categoryName, id);
	        }

	        if (category != null) {
	            isExist = true;

	        }
	        return new ResponseEntity<>(isExist, HttpStatus.OK);
	    }
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("category/addform"); // boleh pake html atau ngga

		Category category = this.categoryRepository.findById(id).orElse(null); // lempar data ke object
		view.addObject("category", category);

		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		Category category = this.categoryRepository.findById(id).orElse(null);

		if (category != null) {
			this.categoryRepository.delete(category);
		}

		return new ModelAndView("redirect:/category/index");

	}

	// USING API //
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi.html");
		return view;
	}

	// USING API + PAGING//
	@GetMapping("indexapi_pg")
	public ModelAndView indexapi_pg() {
		ModelAndView view = new ModelAndView("category/indexapi_pg.html");
		return view;
	}

}
