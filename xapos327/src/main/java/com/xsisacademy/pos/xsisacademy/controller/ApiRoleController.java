package com.xsisacademy.pos.xsisacademy.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.xsisacademy.pos.xsisacademy.model.Role;
import com.xsisacademy.pos.xsisacademy.repository.RoleRepository;

@RestController // API
@RequestMapping("/api/")
public class ApiRoleController {

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("role")
	public ResponseEntity<List<Role>> getAllRole() { // Tergantung data, kalo list untuk banyak

		try {
			// List<Category> listCategory = this.categoryRepsoitory.findAll();
			List<Role> listRole = this.roleRepository.findByisDelete();
			return new ResponseEntity<>(listRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}
	}

	// PAGINATION
	@GetMapping("role/paging")
	public ResponseEntity<Map<String, Object>> getAllRolePages(@RequestParam(defaultValue = "0") int currentPage,
			@RequestParam(defaultValue = "5") int size, @RequestParam("keyword") String keyword,
			@RequestParam("sortType") String sortType) {

		try {
			List<Role> role = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage, size);

			Page<Role> pages;

			if (sortType.equals("ASC")) {
				pages = this.roleRepository.findByisDeleteASC(keyword, false, pagingSort);
			} else {
				pages = this.roleRepository.findByisDeleteDESC(keyword, false, pagingSort);
			}

			role = pages.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pages", pages.getNumber()); // nomor berapa
			response.put("total", pages.getTotalElements()); // berapa banyak
			response.put("total_pages", pages.getTotalPages()); // total maksimal
			response.put("data", role); // list ada disini

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// SAVE
	@PostMapping("role/add")
	public ResponseEntity<Object> saveRole(@RequestBody Role role) {
		role.createdBy = "admin1";
		role.createdDate = new Date();
		role.isDelete = false;

		Role roleData = this.roleRepository.save(role);

		if (roleData.equals(role)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);

		}

	}

	// EDIT
	@GetMapping("role/{id}")
	public ResponseEntity<Object> getRoleyById(@PathVariable("id") Long id) {
		try {
			Optional<Role> role = this.roleRepository.findById(id);
			return new ResponseEntity<>(role, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("role/edit/{id}")
	public ResponseEntity<Object> editRole(@PathVariable("id") Long id, @RequestBody Role role) {
		Optional<Role>roleData = this.roleRepository.findById(id);

		if (roleData.isPresent()) {
			role.id = id;
			role.createdBy = "admin1";
			role.createdDate = new Date();
			role.isDelete = false;
			//category.createBy = categoryData.get().createBy;
			//category.createDate = categoryData.get().createDate;

			this.roleRepository.save(role);
			return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);

		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//DELETE
	@PutMapping("role/delete/{id}")
	public ResponseEntity<Object> deleteRole(@PathVariable("id") Long id){
		Optional<Role> roleData = this.roleRepository.findById(id);
	
		if(roleData.isPresent()) {
			Role role = new Role();
			role.id = id;
			role.roleName =roleData.get().roleName;
			role.isDelete = true;
			role.createdBy = "admin1";
			role.createdDate = new Date();
			role.updatedBy =roleData.get().updatedBy;
			role.updatedDate =roleData.get().updatedDate;
			
			
			
			this.roleRepository.save(role);
			return new ResponseEntity<>("Deleted Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
		
	}
}
