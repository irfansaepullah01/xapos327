package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Variants;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantsRepository;

@Controller
@RequestMapping("/variants/")
public class VariantsController {

		@Autowired
		private VariantsRepository variantsRepository;
		@Autowired
		private CategoryRepository categoryRepository;
		
		@GetMapping("index")

		public ModelAndView index() { //controller melempar data
			ModelAndView view = new ModelAndView("variants/index.html"); // frontend
			
		//untuk melempar data
			List<Variants> listVariants = this.variantsRepository.findAll(); //backend mengambil data dari database
			
			view.addObject("listVariants", listVariants);
			
			return view;
	}
		
		@GetMapping("addform")  //create
		public ModelAndView addform() {
			ModelAndView view = new ModelAndView("variants/addform.html");
			
			
			Variants variants = new Variants();
			view.addObject("variants", variants);
			
			List<Category> listCategory = this.categoryRepository.findAll();
			view.addObject("listCategory", listCategory);
			return view;
		}
		
		@PostMapping("save")
		public ModelAndView save(@ModelAttribute Variants variants, BindingResult result) {
			if(!result.hasErrors()) {
				if(variants.id == null) {
					variants.createBy = "admin1";
					variants.createDate = new Date();
				}
				else {
					Variants tempVariants = this.variantsRepository.findById(variants.id).orElse(null);
					if(tempVariants != null) {
						variants.createBy = tempVariants.createBy;
						variants.createDate = tempVariants.createDate;
						variants.modifyBy = "Admin 1";
						variants.modifyDate = new Date();
						
				}
			}
			this.variantsRepository.save(variants);
			return new ModelAndView("redirect:/variants/index");
		
 			}
			else {
				return new ModelAndView("redirect:/variants/index");
			}
		}
		
		@GetMapping("edit/{id}")
		public ModelAndView edit(@PathVariable("id") Long id) {
			ModelAndView view = new ModelAndView("variants/addform");    //arahkan ke folder variant
			
			Variants variants = this.variantsRepository.findById(id).orElse(null); //lempar data ke object
			view.addObject("variants", variants);
			
			List<Category> listCategory = this.categoryRepository.findAll(); //untuk manggil data dari category
			view.addObject("listCategory", listCategory); //lempar ke front end atau view
			
			return view;
		}
		
		@GetMapping("delete/{id}")
		public ModelAndView delete(@PathVariable("id") Long id) {
			Variants variants = this.variantsRepository.findById(id).orElse(null);
			
			if(variants != null) {
				this.variantsRepository.delete(variants);
			}
			
			return new ModelAndView("redirect:/variants/index");
			
		}
		
		//USING API//
		@GetMapping("indexapi")
		public ModelAndView indexapi(){
			ModelAndView view = new ModelAndView("variants/indexapi.html");
			return view;
		}
		
}
		
		
		
	

