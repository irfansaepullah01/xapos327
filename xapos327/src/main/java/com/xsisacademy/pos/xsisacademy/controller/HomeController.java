package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller   // MVC Controller ke View 
@RequestMapping("/") //Setelah local host slice apa ex ("/category)
public class HomeController {
	@GetMapping("index") //Index
	public String index() {
		return "index.html"; //server
	}
	
	@GetMapping("home") //atribut = home
	public String home() { //method
		return "home.html"; 
	}
	@GetMapping("kalkulator")
	public String kalkulator() {
		return "kalkulator.html";
	}
}

//HomeController = controller + view
//model = store database backend // controller = API (function) // view = frontend